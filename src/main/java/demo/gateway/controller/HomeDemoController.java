package demo.gateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeDemoController {

    @Value("${spring.application.name:n/a}")
    private String appName;

    @GetMapping(value = "/", produces = "text/html")
    public String doHome() {
        StringBuilder html = new StringBuilder();
        html.append("<html>");
        html.append("<head>").append("<title>Azure Spring App : ").append(appName).append("</title>").append("</head>");
        html.append("<body bgcolor=\"#F1F3F4\">");
        html.append("Test API Routes: <br>");
        html.append("<ul>");
        html.append(" <li><a href=\"/app-a/echo\">GET /echo of &quot;spring-app-private-a&quot;</a></li>");
        html.append(" <li><a href=\"/app-b/echo\">GET /echo of &quot;spring-app-private-b&quot;</a></li>");
        html.append(" <li><a href=\"/app-a/config/vars\">GET /config/vars of &quot;spring-app-private-a&quot;</a></li>");
        html.append(" <li><a href=\"/app-b/config/vars\">GET /config/vars of &quot;spring-app-private-b&quot;</a></li>");
        html.append("<ul>");
        html.append("<br>");
        html.append("</body>");
        html.append("</html>");
        return html.toString();
    }
}
