# Spring App Cloud Gateway

### Information

A very basic Springboot Cloud Gateway application

##### As part of series:

* Git repo for Config Server (used by Config Server)
* Spring App Config server (used by this project)
* Spring App Eureka server (used by this project)
* Spring App sample application (uses Config Server & Eureka)

#### Samples:
Serves as a gateway to two "private" services, see "Spring App sample application".
Features of the sample:
- all calls to /a/** are forwarded to Spring app "spring-app-private-a" (see uri: lb://spring-app-private-a)
- all calls to /b/** are forwarded to Spring app "spring-app-private-b" (see uri: lb://spring-app-private-b)
- calls are load-balanced using Eureka (see "Spring App Eureka server")
- an example request HTTP header "X-Request-Foo" is added
- an example response HTTP header "X-Response-Foo" is added
- the prefixes "/a/" and "/b/" are stripped off from the propagated requests, (see StripPrefix=1) 

```yaml
spring:
  application:
    name: spring-app-gateway
  cloud:
    gateway:
      routes:
        - id: routeEchoA
          uri: lb://spring-app-private-a
          predicates:
            - Path=/a/**
          filters:
            - StripPrefix=1
            - AddRequestHeader=X-Request-Foo, Hello API Gateway
            - AddResponseHeader=X-Response-Foo, Hello API Gateway
        - id: routeEchoB
          uri: lb://spring-app-private-a
          predicates:
            - Path=/b/**
          filters:
            - StripPrefix=1
            - AddRequestHeader=X-Request-Foo, Hello API Gateway
            - AddResponseHeader=X-Response-Foo, Hello API Gateway
```

Java Main class is as simple as:

```java
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
````